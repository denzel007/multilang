<?php

namespace App\Models\Crudgen;

class Translations extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $entity_type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $entity_id;

    /**
     *
     * @var string
     * @Column(type="string", length=5, nullable=false)
     */
    public $language;

    /**
     *
     * @var string
     * @Column(type="string", length=48, nullable=false)
     */
    public $key_name;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $value;
    
    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $add;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("scc");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'translations';
    }
    
    public function beforeSave()
    {
        // converting additional fields
        
        $this->add = json_encode($this->add);
    }    

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translations[]|Translations
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translations
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
