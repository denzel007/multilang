<?php 

namespace Tetrapak07\Multilang;

use Phalcon\Db;
use Phalcon\Translate\Adapter;
use Phalcon\Translate\AdapterInterface;
use Phalcon\Translate\Exception;

class DBTranslateAdapter extends Base implements AdapterInterface, \ArrayAccess
{
    /**
     * @var array
     */
    protected $options;
    /**
     * Statement for Exist
     * @var array
     */
    protected $stmtExists;
    /**
     * Statement for Read
     * @var array
     */
    protected $stmtSelect;
    /**
     * Use ICU MessageFormatter to parse message
     *
     * @var boolean
     */
    protected $useIcuMessageFormatter = false;
    
    protected $lang = false;
    /**
     * Class constructor.
     *
     * @param  array $options
     * @throws \Phalcon\Translate\Exception
     */
    public function __construct(array $options)
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }
        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }
        if (!isset($options['language'])) {
            throw new Exception("Parameter 'language' is required");
        }
        if (isset($options['useIcuMessageFormatter'])) {
            if (!class_exists('\MessageFormatter')) {
                throw new Exception('"MessageFormatter" class is required');
            }
            $this->useIcuMessageFormatter = (boolean) $options['useIcuMessageFormatter'];
        }
        $this->stmtSelect = sprintf(
            'SELECT value FROM %s WHERE language = :language AND key_name = :key_name AND entity_type = :entity_type AND entity_id = :entity_id',
            $options['table']
        );
        $this->stmtExists = sprintf(
            'SELECT COUNT(*) AS `count` FROM %s WHERE language = :language AND key_name = :key_name AND entity_type = :entity_type AND entity_id = :entity_id',
            $options['table']
        );
        $this->stmtSelectAll = sprintf(
            'SELECT * FROM %s WHERE language = :language AND entity_type = :entity_type AND entity_id = :entity_id',
            $options['table']
        );
        $this->stmtSelectAllB = sprintf(
            'SELECT * FROM %s WHERE entity_type = :entity_type AND entity_id = :entity_id',
            $options['table']
        );
        $this->stmtExistsAll = sprintf(
            'SELECT COUNT(*) AS `count` FROM %s WHERE language = :language AND entity_type = :entity_type AND entity_id = :entity_id',
            $options['table']
        );
        $this->options = $options;
    }
    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @param  array  $placeholders
     * @return string
     */
    public function query($translateKey, $type = '', $id = 0, $placeholders = null)
    {
        $options = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
        }
        $translation = $options['db']->fetchOne(
            $this->stmtSelect,
            Db::FETCH_ASSOC,
            ['language' => $options['language'], 'key_name' => $translateKey, 'entity_type' => $type, 'entity_id' => $id]
        );
        #$value  = empty($translation['value']) ? $translateKey : $translation['value'];
        $value  = empty($translation['value']) ? false : $translation['value'];
        if (is_array($placeholders) && !empty($placeholders)) {
            if (true === $this->useIcuMessageFormatter) {
                $value = \MessageFormatter::formatMessage($options['language'], $value, $placeholders);
            } else {
                foreach ($placeholders as $placeHolderKey => $placeHolderValue) {
                    $value = str_replace('%' . $placeHolderKey . '%', $placeHolderValue, $value);
                }
            }
        }
        return $value;
    }
    
     /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @param  array  $placeholders
     * @return string
     */
    public function queryAll($type = '', $id = 0, $placeholders = null)
    {
        $result = [];
        $options     = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
           $opt = ['language' => $options['language'], 'entity_type' => $type, 'entity_id' => $id];
           $q = $this->stmtSelectAll;
        } else {
           $options['language'] =  false;
           $opt = ['entity_type' => $type, 'entity_id' => $id];
           $q = $this->stmtSelectAllB;
        }
        
        $translations = $options['db']->fetchAll(
            $q,
            Db::FETCH_ASSOC,
            $opt
        );
        foreach ($translations as $translation) {
            $value = empty($translation['value']) ? '' : $translation['value'];
            if (is_array($placeholders) && !empty($placeholders) && $options['language']) {
                if (true === $this->useIcuMessageFormatter) {
                    if ($options['language']) {
                    $value = \MessageFormatter::formatMessage($options['language'], $value, $placeholders);
                    }
                } else {
                    foreach ($placeholders as $placeHolderKey => $placeHolderValue) {
                        $value = str_replace('%' . $placeHolderKey . '%', $placeHolderValue, $value);
                    }
                }
            }
            $result[$translation['language']][$translation['key_name']] = $value;
        }
        return $result;
    }
    /**
     * Returns the translation string of the given key
     *
     * @param  string $translateKey
     * @param  array  $placeholders
     * @return string
     */
    // @codingStandardsIgnoreStart
    public function _($translateKey, $type = '', $id = 0, $placeholders = null)
    {
        return $this->query($translateKey, $type, $id, $placeholders);
    }
    // @codingStandardsIgnoreEnd
    /**
     * {@inheritdoc}
     *
     * @param  string  $translateKey
     * @return boolean
     */
    public function exists($translateKey, $type = '', $id = 0)
    {
        $options = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
        }
        $result = $options['db']->fetchOne(
            $this->stmtExists,
            Db::FETCH_ASSOC,
            ['language' => $options['language'], 'key_name' => $translateKey, 'entity_type' => $type, 'entity_id' => $id]
        );
        return !empty($result['count']);
    }
    /**
     * Adds a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function add($translateKey, $message, $type = '', $id = 0)
    {
        $options = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
        }
        $data = ['language' => $options['language'], 'key_name' => $translateKey, 'entity_type' => $type, 'entity_id' => $id, 'value' => $message];
        return $options['db']->insert($options['table'], array_values($data), array_keys($data));
    }
    /**
     * Update a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function update($translateKey, $message, $type = '', $id = 0)
    {
        $options = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
        }
        return $options['db']->update($options['table'], ['value'], [$message], [
            'conditions' => 'key_name = ? AND language = ? AND entity_type = ? AND entity_id = ?',
            'bind' =>  ['key' => $translateKey, 'lang' => $options['language'], 'type' => $type, 'id' => $id]
        ]);
    }
    /**
     * Deletes a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @return boolean
     */
    public function delete($translateKey, $type = '', $id = 0)
    {
        $options = $this->options;
        if($this->lang) {
           $options['language'] =  $this->lang;
        }
        return $options['db']->delete(
            $options['table'],
            'key_name = :key AND language = :lang AND entity_type = :type AND entity_id = :id',
            ['key' => $translateKey, 'lang' => $options['language'], 'type' => $type, 'id' => $id]
        );
    }
    /**
     * Sets (insert or updates) a translation for given key
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function set($translateKey, $message, $type = '', $id = 0)
    {
        return $this->exists($translateKey, $type, $id) ?
            $this->update($translateKey, $message, $type, $id) : $this->add($translateKey, $message, $type, $id);
    }
    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @return string
     */
    public function offsetExists($translateKey, $type = '', $id = 0)
    {
        return $this->exists($translateKey, $type, $id);
    }
    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @param  string $message
     * @return string
     */
    public function offsetSet($translateKey, $message, $type = '', $id = 0, $lang = false)
    {
        $this->lang = $lang; 
        return $this->set($translateKey, $message, $type, $id);
    }
    /**
     * {@inheritdoc}
     *
     * @param string $translateKey
     * @return string
     */
    public function offsetGet($translateKey, $type = '', $id = 0, $lang = false)
    {
        $this->lang = $lang; 
        return $this->query($translateKey, $type, $id);
    }
    
     /**
     * {@inheritdoc}
     *
     * @param string $translateKey
     * @return string
     */
    public function offsetGetAll($type = '', $id = 0, $lang = false)
    {
        $this->lang = $lang; 
        return $this->queryAll($type, $id);
    }
    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @return string
     */
    public function offsetUnset($translateKey, $type = '', $id = 0, $lang = false)
    {
        $this->lang = $lang; 
        return $this->delete($translateKey, $type, $id);
    }
}